# Cellular Automaton

Visual Representation of an image after n amount of cellular automaton cycles.

# Instructions
1.  Code in Matlab: Use Matlab or any other IDE capable of running Matlab scripts
2.  Enter Run the script
3.  Enter the name of the image (for the moment only works with jpg)
4.  Choose the automaton nerighborhood method: 1 for Moore and 2 for Von Neumann
5.  Choose How many cycles you would like the automaton pattern to repeat for
6.  Let the script run and display the output image