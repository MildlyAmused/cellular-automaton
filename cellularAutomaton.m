%while (valid ~= 0)
    
    % Ask the user where the target image is located
    % Note: will only load png files located in the same directory as the
    % code
    prompt = 'Please enter the image name and extension\n';
    location = input(prompt, 's');
    [temp, cmap] = imread(location);
    [verticalPixels, horizontalPixels, channels] = size(temp);
    %img = ind2rgb(temp, cmap);
    %Note: verticalPixels = row size, horizontalPixels = column size
    red = temp(:,:,1);
    green = temp(:,:,2);
    blue = temp(:,:,3);
    % Ask the user for which feature extraction to be used
    prompt = 'Which cellular automaton neighborhood would you like to choose?\n1 for Moore neighborhood\n2 for Von Neumann neighborhood\n';
    cell_method = input(prompt);
    prompt = 'How many iterations would you like to perform?\n';
    maxIteration = input(prompt);
    pictureCount = 1;
    
    % Initially, we start with with an m x n x channel matrix 
    % of ones with the same size as the original image. Every  
    % one corresponds to a true for a given pixel's value.
    % I.E. a one corresponds to a non-inverted color and a 
    % zero is an inverted color
    trueTracker = ones(verticalPixels, horizontalPixels);
    
    % Introduce an impurity within the center of the image. If the pixel
    % count is even for a dimmension, modify the two middle pixels.
    
    if mod(verticalPixels, 2) == 0 % Even number of vertical pixels
        vertiMid = verticalPixels/2;
        if mod(horizontalPixels, 2) == 0 % Even number of horizontal pixels
            horiMid = horizontalPixels/2;
            %{
            red(vertiMid:vertiMid+1,horiMid:horiMid+1) = 255 ...
            - red(vertiMid:vertiMid+1,horiMid:horiMid+1);
            green(vertiMid:vertiMid+1,horiMid:horiMid+1) = 255 ...
            - green(vertiMid:vertiMid+1,horiMid:horiMid+1);
            blue(vertiMid:vertiMid+1,horiMid:horiMid+1) = 255 ...
            - blue(vertiMid:vertiMid+1,horiMid:horiMid+1);
            %}
            trueTracker(vertiMid:vertiMid+1,horiMid:horiMid+1) = 0; 
        else % Odd number of horizontal pixels
            horiMid = ceil(horizontalPixels/2);
            %{
            red(vertiMid:vertiMid+1,horiMid) = 255 ...
            - red(vertiMid:vertiMid+1,horiMid);
            green(vertiMid:vertiMid+1,horiMid) = 255 ...
            - green(vertiMid:vertiMid+1,horiMid);
            blue(vertiMid:vertiMid+1,horiMid) = 255 ...
            - blue(vertiMid:vertiMid+1,horiMid);
            %}
            trueTracker(vertiMid:vertiMid+1,horiMid) = 0;
        end
        
    else % Odd number of vertical pixels
        vertiMid = ceil(verticalPixels/2);
        if mod(horizontalPixels, 2) == 0 % Even number of horizontal pixels
            horiMid = horizontalPixels/2;
            %{
            red(vertiMid:,horiMid:horiMid+1) = 255 ...
            - red(vertiMid,horiMid:horiMid+1);
            green(vertiMid,horiMid:horiMid+1) = 255 ...
            - green(vertiMid,horiMid:horiMid+1);
            blue(vertiMid,horiMid:horiMid+1) = 255 ...
            - blue(vertiMid,horiMid:horiMid+1);
            %}
            trueTracker(vertiMid,horiMid:horiMid+1) = 0;
        else % Odd number of horizontal pixels
            horiMid = ceil(horizontalPixels/2);
            %{
            red(vertiMid:,horiMid) = 255 ...
            - red(vertiMid,horiMid);
            green(vertiMid,horiMid) = 255 ...
            - green(vertiMid,horiMid);
            blue(vertiMid,horiMid) = 255 ...
            - blue(vertiMid,horiMid);
            %}
            trueTracker(vertiMid,horiMid) = 0;
        end
        
    end
    
    switch cell_method
        case 1 % Moore
            
            for i = 1:maxIteration
              
              for j = 2:verticalPixels-1
                
                for k = 2:horizontalPixels-1
                  
                  %if (j > 1 && k > 1 && j < verticalPixels && k < horizontalPixels)
                    sum = trueTracker(j-1,k-1); %bottom left
                    sum = sum + trueTracker(j,k-1); %middle left
                    sum = sum + trueTracker(j+1,k-1); %top left
                    sum = sum + trueTracker(j+1,k); %middle top
                    sum = sum + trueTracker(j+1,k+1); %top right
                    sum = sum + trueTracker(j,k+1); %middle right
                    sum = sum + trueTracker(j-1,k+1); %bottom right
                    sum = sum + trueTracker(j-1,k); %bottom middle
                    if (mod(sum, 2) == 1)
                      trueTracker(j,k) = 1;
                    else
                      trueTracker(j,k) = 0;
                    end
                    
                  %end
                  
                end
                
              end
              
            end
            
            for j = 1:verticalPixels
              
              for k = 1:horizontalPixels
                
                if (trueTracker(j,k) == 0)
                  red(j,k) = 255 - red(j,k);
                  green(j,k) = 255 - green(j,k);
                  blue(j,k) = 255 - blue(j,k);
                end
              
              end
              
            end
            
        case 2 % Von Neumann
            
            for i = 1:maxIteration
              
              for j = 2:verticalPixels-1
                
                for k = 2:horizontalPixels-1
                  
                  %if (j > 1 && k > 1 && j < verticalPixels && k < horizontalPixels)
                    sum = trueTracker(j-1,k); %bottom
                    sum = sum + trueTracker(j,k-1); %left
                    sum = sum + trueTracker(j+1,k); %top
                    sum = sum + trueTracker(j,k+1); %right
                    if (mod(sum, 2) == 1)
                      trueTracker(j,k) = 1;
                    else
                      trueTracker(j,k) = 0;
                    end
                    
                  %end
                  
                end
                
              end
              
            end
            
            for j = 1:verticalPixels
              
              for k = 1:horizontalPixels
                
                if (trueTracker(j,k) == 0)
                  red(j,k) = 255 - red(j,k);
                  green(j,k) = 255 - green(j,k);
                  blue(j,k) = 255 - blue(j,k);
                end
              
              end
              
            end
            
    end
    temp = cat(3,red,green,blue);
    [temp, cmap] = rgb2ind(temp,65536);
    imshow(temp,cmap);